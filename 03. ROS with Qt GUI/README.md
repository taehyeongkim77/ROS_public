# Build

```c
$ catkin_create_qt_pkg <패키지명> <br>
```

다음과 같은 방법으로 패키지를 생성하면 ~/아래에 패키지가 생성됨.<br>
생성된 패키지를 catkin_ws/src내에 넣고<br>
$ cm 하면 빌드 끝.

---

# Run

```c
$ rosrun <패키지명> <패키지명>
```

---

# ros with qt 설치
먼저 qt - creator를 먼저 설치해야함 [홈페이지](https://www.qt.io/download#section-2)<br>

```c
# kinetic
$ sudo apt-get install ros-kinetic-qt-create<br>
$ sudo apt-get install ros-kinetic-qt-build<br>
```


```c
# ROS indigo
$ sudo apt-get install ros-indigo-qt-create<br>
$ sudo apt-get install ros-indigo-qt-build<br>
```

---

# package 생성
$ catkin_create_qt_pkg <패키지명> <br>