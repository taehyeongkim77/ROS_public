# ROS 설치 (Ubuntu 14.04기준)
$ wget https://gitlab.com/taehyeongkim77/ROS_public/raw/master/00.%20CONUSV_ROS_tools/install_ros_indigo.sh <br>

$ chmod 755 ./install_ros_indigo.sh<br>

$ bash ./install_ros_indigo.sh<br>

---

### 추가 설치사항

`Git 설치하기`

```c
$ sudo apt-get install git-core
```

<br>

`smartGit 설치하기`
smartgit-linux-18_1_5.tar.gz 다운로드 후 bin파일내의 .sh파일을 실행해야함.
```c
$ bash smartgit.sh
```
[홈페이지](https://www.syntevo.com/smartgit/)
